
param f_pv default 0;

subject to f_pv_constr:
	sum {d in DISTRICTS} F_mult_dis[d,'pv_dis'] + sum {pv in {'PV_LV','PV_MV','PV_HV','PV_EHV'}}F_Mult[pv] = f_pv;
	#sum {pv in {'PV_LV','PV_MV','PV_HV','PV_EHV'}}F_Mult[pv] = f_pv;