# Power to the People - On the Role of Districts in Decentralized Energy Systems

## Abstract

In the face of escalating climate concerns and the push for sustainable development, the global shift towards renewable
and decentralized energy systems presents new challenges and opportunities. This study investigates integrating
decentralized energy production, particularly photovoltaic (PV) systems, into national energy planning, aiming to
optimize energy strategies that balance local production and consumption with national objectives. By analyzing the
Swiss energy model, the research employs the EnergyScope and REHO models to assess the strategic implications of
decentralized versus centralized energy systems. Results show that a decentralized approach can significantly reduce PV
installation needs to 35 GW, about 23% of potential capacity, and decrease system costs by 10% to CHF 1230 per capita.
This strategy emphasizes local consumption, minimizes grid reinforcement demands, and leverages economic advantages
while addressing overproduction challenges through effective energy storage and grid management. Conclusions underline
the strategic value of combining centralized and decentralized methods for resilient and sustainable energy planning.
The study contributes to the discourse on energy policy and infrastructure planning, advocating for a hybrid model that
accommodates both local conditions and broader energy objectives, urging further research into climate impacts and
technology integration for a comprehensive energy future.

## Introduction

This repository contains the model and result data for the corresponding paper.

## Structure

        .
    ├── 01_Model                                            # EnergyScope model (AMPL)
    ├── 02_Data                                             # Results data for the corresponding subsections
    │   ├── 2.1_PV_Parametrization            
    │   └── 2.2_SC_Parametrization      
    └── README.md


## Authors and acknowledgment
- [**Jonas Schnidrig**](mailto:jonas.schnidrig@epfl.ch): Main author
- **Arthur Chuat**: Validation, Coding and Review
- **Cédric Terrier**: Validation, Coding and Review
- **François Maréchal**: Study conceptualization
- **Manuele Margni**: Study conceptualization and Review


## License
MIT Licence

## Project status
Published in Energies 2024, 17(7), 1718; https://doi.org/10.3390/en17071718 
